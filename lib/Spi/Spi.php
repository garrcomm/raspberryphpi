<?php

namespace Garrcomm\RaspberryPhpi\Spi;

interface Spi
{
    public function write(string $data): self;
}
