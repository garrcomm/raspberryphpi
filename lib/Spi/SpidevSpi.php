<?php

namespace Garrcomm\RaspberryPhpi\Spi;

/**
 * Direct communication to the SPI bus through a file stream
 *
 * Pins are documented at https://www.raspberrypi.org/documentation/hardware/raspberrypi/spi/README.md#hardware
 */
class SpidevSpi implements Spi
{
    /**
     * Prefix for the SPI device
     *
     * @var string
     */
    public static $devicePrefix = '/dev/spidev';

    /**
     * SPI ioctl constants
     *
     * For a full list, see https://docs.huihoo.com/doxygen/linux/kernel/3.7/spidev_8h.html
     */
    private const SPI_IOC_WR_MAX_SPEED_HZ = 1074031364;

    /**
     * Path to the device
     *
     * @var string
     */
    private $device;

    /**
     * The speed of the SPI bus in hz. When not set, the system default will be used.
     *
     * @var int|null
     */
    private $maxSpeedHz = null;

    public function __construct(int $bus = 0, int $chipSelect = 0)
    {
        $device = static::$devicePrefix . $bus . '.' . $chipSelect;

        if (!file_exists($device)) {
            throw new \RuntimeException(
                'Device not found. Make sure the path is correct and SPI is enabled (start raspi-config to enable)'
            );
        }
        if (!is_writable($device)) {
            throw new \RuntimeException('Device not writable. Make sure we have the correct permissions');
        }

        $this->device = $device;
    }

    /**
     * Set the maximum SPI speed in Hz
     *
     * @param int $hz
     *
     * @return Spi
     */
    public function setMaxSpeed(int $hz): Spi
    {
        if (!function_exists('ioctl')) {
            throw new \RuntimeException(
                'Can\'t overwrite SPI speed without ext-ioctl. See stubs/ext-ioctl.php for details.'
            );
        }
        $this->maxSpeedHz = $hz;

        return $this;
    }

    public function write(string $data): Spi
    {
        $fp = fopen($this->device, 'w');
        if ($this->maxSpeedHz !== null) {
            ioctl($fp, static::SPI_IOC_WR_MAX_SPEED_HZ, $this->maxSpeedHz);
        }
        fwrite($fp, $data);
        fclose($fp);

        return $this;
    }
}
