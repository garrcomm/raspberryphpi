<?php

namespace Garrcomm\RaspberryPhpi\Spi;

use Garrcomm\RaspberryPhpi\Gpio\Gpio;

/**
 * Bitbanging SPI on selected ports
 *
 * See also https://circuitdigest.com/article/introduction-to-bit-banging-spi-communication-in-arduino-via-bit-banging
 *
 * Warning! The clock speed is never 100% accurate with bitbanging!!
 */
class BitbangSpi implements Spi
{
    /**
     * Reference to the clock pin
     *
     * @var Gpio
     */
    private $spiClock;
    /**
     * Reference to the MOSI (Master Out, Slave In) pin
     *
     * @var Gpio|null
     */
    private $spiMosi;
    /**
     * Reference to the MISO (Master In, Slave Out) pin
     *
     * @var Gpio|null
     */
    private $spiMiso;
    /**
     * Reference to the SS (Slave Select) pin
     * @var Gpio|null
     */
    private $spiSelect;

    /**
     * Initializes a new SPI bus
     *
     * @param Gpio      $spiClock   Reference to the clock pin
     * @param Gpio|null $spiMosi    Reference to the MOSI (Master Out, Slave In) pin
     * @param Gpio|null $spiMiso    Reference to the MISO (Master In, Slave Out) pin
     * @param Gpio|null $spiSelect  Reference to the SS (Slave Select) pin
     */
    public function __construct(
        Gpio $spiClock,
        Gpio $spiMosi = null,
        Gpio $spiMiso = null,
        Gpio $spiSelect = null
    ) {
        $this->spiClock  = $spiClock;
        $this->spiMosi   = $spiMosi;
        $this->spiMiso   = $spiMiso;
        $this->spiSelect = $spiSelect;

        $this->spiClock
            ->setDirection(Gpio::DIRECTION_OUT)
            ->setValue(Gpio::VALUE_LOW);

        if ($this->spiMosi) {
            $this->spiMosi
                ->setDirection(Gpio::DIRECTION_OUT)
                ->setValue(Gpio::VALUE_LOW);
        }

        if ($this->spiMiso) {
            $this->spiMosi->setDirection(Gpio::DIRECTION_IN);
        }

        if ($this->spiSelect) {
            $this->spiSelect
                ->setDirection(Gpio::DIRECTION_OUT)
                ->setValue(Gpio::VALUE_HIGH);
        }
    }

    public function write(string $data): Spi
    {
        if (!$this->spiMosi) {
            throw new \RuntimeException('Can\'t write without MOSI pin defined');
        }

        // Convert data string to an array of bit-bools
        $bytes = str_split($data);
        array_walk($bytes, function (&$in) {
            $in = ord($in);
        });
        $bits = array();
        foreach ($bytes as $byte) {
            for ($bit = 0; $bit < 8; ++$bit) {
                $bits[] = (pow(2, $bit) & $byte) ? Gpio::VALUE_HIGH : Gpio::VALUE_LOW;
            }
        }

        // Select correct slave device
        if ($this->spiSelect) {
            $this->spiSelect->setValue(Gpio::VALUE_LOW);
        }

        // Write all bytes
        foreach ($bits as $bit) {
            $this->spiMosi->setValue($bit);
            $this->spiClock->setValue(Gpio::VALUE_HIGH);
            $this->spiClock->setValue(Gpio::VALUE_LOW);
            $this->spiMosi->setValue(Gpio::VALUE_LOW);
        }

        // Free the selected slave device
        if ($this->spiSelect) {
            $this->spiSelect->setValue(Gpio::VALUE_HIGH);
        }

        return $this;
    }
}
