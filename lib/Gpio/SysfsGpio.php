<?php

namespace Garrcomm\RaspberryPhpi\Gpio;

/**
 * GPIO pins driven through Sysfs
 *
 * See https://www.kernel.org/doc/Documentation/gpio/sysfs.txt for more details
 */
class SysfsGpio implements Gpio
{
    /**
     * Path to Sysfs, can be overwritten, but must end on a slash
     *
     * @var string
     */
    public static $sysfsRootPath = '/sys/class/gpio/';

    /**
     * Pin number for this GPIO pin
     *
     * @var int
     */
    private $pinNo;

    /**
     * Path to the GPIO pin in the Sysfs root
     *
     * @var string
     */
    private $sysfsGpioPath;

    public function __construct(int $pinNo)
    {
        $this->pinNo = $pinNo;
        $this->sysfsGpioPath = static::$sysfsRootPath . 'gpio' . $pinNo . '/';

        if (!file_exists(static::$sysfsRootPath . 'export')) {
            throw new \RuntimeException('Can\'t find Sysfs exporter');
        }

        file_put_contents(static::$sysfsRootPath . 'export', $pinNo);

        // Permissions checks
        if (!file_exists($this->sysfsGpioPath)) {
            throw new \RuntimeException('Can\'t export GPIO pin. Do we have correct permissions?');
        }
        if (!is_writable($this->sysfsGpioPath . 'direction')) {
            $this->__destruct();
            throw new \RuntimeException('Can\'t change GPIO pin direction. Do we have correct permissions?');
        }
    }

    public function __destruct()
    {
        file_put_contents(static::$sysfsRootPath . 'unexport', $this->pinNo);
    }

    public function getDirection(): int
    {
        $strDirection = trim(file_get_contents($this->sysfsGpioPath . 'direction'));
        if ($strDirection == 'out') {
            return static::DIRECTION_OUT;
        }
        if ($strDirection == 'in') {
            return static::DIRECTION_IN;
        }

        throw new \RuntimeException('Invalid direction received from GPIO');
    }

    public function setDirection(int $direction): Gpio
    {
        $strDirection = null;
        if ($direction == static::DIRECTION_IN) {
            $strDirection = 'in';
        } elseif ($direction == static::DIRECTION_OUT) {
            $strDirection = 'out';
        }

        if ($strDirection === null) {
            throw new \InvalidArgumentException(
                '$direction is invalid. Use one of the DIRECTION_IN or DIRECTION_OUT constants'
            );
        }

        file_put_contents($this->sysfsGpioPath . 'direction', $strDirection);

        return $this;
    }

    public function getValue(): bool
    {
        return trim(file_get_contents($this->sysfsGpioPath . 'value')) === '1';
    }

    public function setValue(bool $value): Gpio
    {
        file_put_contents($this->sysfsGpioPath . 'value', $value ? '1' : '0');

        return $this;
    }

    public function getActiveLow(): bool
    {
        return trim(file_get_contents($this->sysfsGpioPath . 'active_low')) === '1';
    }

    public function setActiveLow(bool $value): Gpio
    {
        file_put_contents($this->sysfsGpioPath . 'active_low', $value ? '1' : '0');

        return $this;
    }
}
