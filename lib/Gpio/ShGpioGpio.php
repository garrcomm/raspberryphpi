<?php

namespace Garrcomm\RaspberryPhpi\Gpio;

/**
 * GPIO pins driven through the  GPIO Interface library for the Raspberry Pi
 *
 * See http://wiringpi.com/the-gpio-utility/ for more details
 */
class ShGpioGpio implements Gpio
{
    /**
     * Path to the GPIO shell command
     *
     * @var string
     */
    public static $shGpio = '/usr/bin/gpio';

    /**
     * The pin number
     *
     * @var int
     */
    private $pinNo;

    /**
     * Value for the active low setting; this is emulated since the gpio tool doesn't support this as far as I know.
     *
     * @var bool
     */
    private $activeLow = false;

    /**
     * Let's remember the direction
     *
     * @var int
     */
    private $direction = self::DIRECTION_IN;

    public function __construct(int $pinNo)
    {
        $this->pinNo = $pinNo;

        if (!file_exists(static::$shGpio)) {
            throw new \RuntimeException('Can\'t find ' . static::$shGpio);
        }

        $this->execute('export ' . $pinNo . ' out');
    }

    public function __destruct()
    {
        $this->execute('unexport ' . $this->pinNo);
    }

    public function getDirection(): int
    {
        return $this->direction;
    }

    public function setDirection(int $direction): Gpio
    {
        $strDirection = null;
        if ($direction == self::DIRECTION_IN) {
            $strDirection = 'in';
        } elseif ($direction == self::DIRECTION_OUT) {
            $strDirection = 'out';
        }

        if ($strDirection === null) {
            throw new \InvalidArgumentException(
                '$direction is invalid. Use one of the DIRECTION_IN or DIRECTION_OUT constants'
            );
        }

        $this->direction = $direction;
        $this->execute('-g mode ' . $this->pinNo . ' ' . $strDirection);

        return $this;
    }

    public function getValue(): bool
    {
        return $this->handleActiveLow($this->execute('-g read ' . $this->pinNo) == '1');
    }

    public function setValue(bool $value): Gpio
    {
        $this->execute('-g write ' . $this->pinNo . ' ' . ($this->handleActiveLow($value) ? '1' : '0'));

        return $this;
    }

    public function getActiveLow(): bool
    {
        return $this->activeLow;
    }

    public function setActiveLow(bool $value): Gpio
    {
        $this->activeLow = $value;

        return $this;
    }

    private function execute(string $command): string
    {
        exec(static::$shGpio . ' ' . $command . ' 2>&1', $output, $retValue);
        $return = implode(PHP_EOL, $output);
        if ($retValue !== 0) {
            throw new \RuntimeException('Invalid response: ' . $return, $retValue);
        }
        return $return;
    }

    private function handleActiveLow(bool $value): bool
    {
        return $this->activeLow ? !$value : $value;
    }
}
