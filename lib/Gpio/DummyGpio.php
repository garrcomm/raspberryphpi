<?php

namespace Garrcomm\RaspberryPhpi\Gpio;

/**
 * Dummy GPIO; works like a real GPIO pin, but doesn't actually do a thing. Useful for testing purposes.
 */
class DummyGpio implements Gpio
{
    /**
     * Method for logging purposes
     *
     * @var callable|null
     */
    private $outputMethod;

    /**
     * Pin number for this GPIO pin
     *
     * @var int
     */
    private $pinNo;

    /**
     * Storage for the direction
     *
     * @var int
     */
    private $direction = self::DIRECTION_IN;

    /**
     * Storage for the value
     *
     * @var bool
     */
    private $value = self::VALUE_HIGH;

    /**
     * Storage for the active low setting
     *
     * @var bool
     */
    private $activeLow = false;

    public function __construct(int $pinNo)
    {
        $this->pinNo = $pinNo;
    }

    public function getDirection(): int
    {
        return $this->direction;
    }

    public function setDirection(int $direction): Gpio
    {
        if (!in_array($direction, [self::DIRECTION_IN, self::DIRECTION_OUT])) {
            throw new \InvalidArgumentException(
                '$direction is invalid. Use one of the DIRECTION_IN or DIRECTION_OUT constants'
            );
        }
        $this->output('D→' . ($direction === self::DIRECTION_IN ? 'i' : 'o'));
        $this->direction = $direction;
        return $this;
    }

    public function getValue(): bool
    {
        return $this->value;
    }

    public function setValue(bool $value): Gpio
    {
        $this->output('V→' . ($value ? '1' : '0'));
        $this->value = $value;
        return $this;
    }

    public function getActiveLow(): bool
    {
        return $this->activeLow;
    }

    public function setActiveLow(bool $value): Gpio
    {
        $this->output('!→' . ($value ? '1' : '0'));
        $this->activeLow = $value;
        return $this;
    }

    private function output(string $text): void
    {
        if ($this->outputMethod !== null) {
            call_user_func($this->outputMethod, $this->pinNo . ':' . $text);
        }
    }

    /**
     * When using the dummy pin for debugging, set a callable that can log pin changes
     *
     * @param callable|null $outputMethod
     *
     * @return Gpio
     */
    public function setOutputMethod(callable $outputMethod = null): Gpio
    {
        $this->outputMethod = $outputMethod;
        return $this;
    }
}
