<?php

namespace Garrcomm\RaspberryPhpi\Gpio;

interface Gpio
{
    public const DIRECTION_IN = 1;
    public const DIRECTION_OUT = 2;

    public const VALUE_LOW = false;
    public const VALUE_HIGH = true;

    /**
     * Initializes the GPIO pin
     *
     * @param int $pinNo
     */
    public function __construct(int $pinNo);

    /**
     * Returns one of the constants DIRECTION_IN or DIRECTION_OUT
     *
     * @return int
     */
    public function getDirection(): int;

    /**
     * Set the direction
     *
     * @param int $direction One of the constants DIRECTION_IN or DIRECTION_OUT
     *
     * @return $this
     */
    public function setDirection(int $direction): self;

    /**
     * Get the current value, for readability, constants VALUE_HIGH and VALUE_LOW may be used.
     *
     * @return bool
     */
    public function getValue(): bool;

    /**
     * Set the current value
     *
     * @param bool $value New value, for readability, constants VALUE_HIGH and VALUE_LOW may be used.
     *
     * @return $this
     */
    public function setValue(bool $value): self;

    /**
     * Get the current Active Low value (in simple terms: invert the high/low values on a hardware level), true or false
     *
     * @return bool
     */
    public function getActiveLow(): bool;

    /**
     * Set the current Active Low value (in simple terms: invert the high/low values on a hardware level)
     *
     * @param bool $value New value, true or false
     *
     * @return $this
     */
    public function setActiveLow(bool $value): self;
}
