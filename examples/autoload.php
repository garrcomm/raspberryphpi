<?php

// This file just includes all library files. Not really autoloading but simulates as if they're autoloaded

require '../lib/Gpio/Gpio.php';
require '../lib/Gpio/SysfsGpio.php';
require '../lib/Gpio/ShGpioGpio.php';
require '../lib/Gpio/DummyGpio.php';
require '../lib/Spi/Spi.php';
require '../lib/Spi/BitbangSpi.php';
require '../lib/Spi/SpidevSpi.php';
