<?php

use Garrcomm\RaspberryPhpi\Gpio\Gpio;
use Garrcomm\RaspberryPhpi\Gpio\SysfsGpio;
use Garrcomm\RaspberryPhpi\Gpio\ShGpioGpio;
use Garrcomm\RaspberryPhpi\Gpio\DummyGpio;

require __DIR__ . '/autoload.php';

/**
 * GPIO Speed test example
 *
 * Configure this example by populating the $pins array with GPIO pin numbers.
 * Those pin numbers will be driven on and off with "active low" setting, with all different GPIO approaches.
 */
$pins = array(1, 12, 13);

function formatMicrotime(float $microTime = null): string
{
    if ($microTime === null) {
        $microTime = microtime(true);
    }
    return date('Y-m-d H:i:s', $microTime) . substr($microTime, strpos($microTime, '.'));
}

foreach ([SysfsGpio::class, ShGpioGpio::class, DummyGpio::class] as $gpioDriver) {
    $start = microtime(true);
    echo '[' . formatMicrotime($start) . '] Testing speed ' . $gpioDriver . PHP_EOL;

    foreach ($pins as $pinNo) {
        $gpio = new $gpioDriver($pinNo); /* @var $gpio Gpio */
        $gpio
            ->setActiveLow(true)
            ->setDirection(Gpio::DIRECTION_OUT)
            ->setValue(Gpio::VALUE_HIGH);

        sleep(1);

        echo '[' . formatMicrotime() . '] Pin ' . $pinNo;
        echo ' active low: ' . var_export($gpio->getActiveLow(), true);
        echo ', direction: ' . var_export($gpio->getDirection(), true);
        echo ', value: ' . var_export($gpio->getValue(), true);
        echo PHP_EOL;
    }

    $end = microtime(true);
    echo '[' . formatMicrotime($end) . '] Changing 3 GPIOs with 3s delay took ' .
        round($end - $start, 4) . 'sec' . PHP_EOL . PHP_EOL;
}
