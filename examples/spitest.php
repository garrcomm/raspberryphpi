<?php

use Garrcomm\RaspberryPhpi\Spi\SpidevSpi;
use Garrcomm\RaspberryPhpi\Gpio\SysfsGpio;
use Garrcomm\RaspberryPhpi\Spi\BitbangSpi;

require __DIR__ . '/autoload.php';

/**
 * SPI test example
 *
 * This test is done with a string of RGB LEDs driven with WS2801 chips in a chain
 * (use your favorite search engine on that chip and be enlightened)
 *
 * Connect the LED lights to +5V, Gnd, and pins 10 & 11 (SPI MOSI and SCLK)
 * Then you can configure the amount of LEDs below, some colors, and if you want to use the native driver or bitbanging.
 */
$ledCount = 75;
$nativeSpiDriver = true;
$off             = chr(0)   . chr(0)   . chr(0);
$color['red']    = chr(255) . chr(0)   . chr(0);
$color['green']  = chr(0)   . chr(255) . chr(0);
$color['blue']   = chr(0)   . chr(0)   . chr(255);
$color['yellow'] = chr(255) . chr(255) . chr(0);
$color['white']  = chr(255) . chr(255) . chr(255);

// Initialize the SPI bus
if ($nativeSpiDriver) {
    $spi = new SpidevSpi(0, 0);
    $spi->setMaxSpeed(10);
} else {
    $spi = new BitbangSpi(
        new SysfsGpio(11),
        new SysfsGpio(10)
    );
}

// Set all colors
foreach ($color as $colorName => $colorData) {
    echo $colorName . PHP_EOL;
    $leds = str_repeat($colorData, $ledCount);
    $spi->write($leds);
    sleep(2);
}

// Glue all colors in a row
$leds = '';
while (strlen($leds) < ($ledCount * 3)) {
    $leds .= implode('', $color);
}
$leds = substr($leds, 0, $ledCount * 3);

// Repeat program 6 times
for ($i = 0; $i < 6; ++$i) {
    // Make all LEDs emit specific colors
    echo 'ON' . PHP_EOL;
    $spi->write($leds);

    // Shift last 3 bytes (1 LED) to the beginning
    $leds = substr($leds, -3) . substr($leds, 0, -3);

    // Wait for 1 second
    sleep(1);
}

// All off now
echo 'OFF' . PHP_EOL;
$spi->write(str_repeat($off, $ledCount));
