# RaspberryPhpi library

**A PHP library for the Raspberry Pi**

This library adds support to PHP for the Raspberry Pi GPIO devices.
I've started programming this library for my own (experimental) use.
With this library I did some testing in performance between different ways of driving pins, and connected some basic hardware parts.

## Different GPIO approaches

Driving GPIOs can be done in different ways on the Raspberry Pi. The easiest way is with the `gpio` console tool.
But I found that executing the console tool is not very fast. For that reason I also wrote a `sysfs` variant that uses file streams instead.

With some testing (see [examples/speedtest.php](examples/speedtest.php)), I found that `sysfs` is the fastest approach.
The downside is that it requires write permissions to hardware devices; thus only available when running the script as root.

When not having correct permissions, the `gpio` console tool is a better (but slower) way to go.

In the future, I want to make a RaspberryPhpi object that automatically selects the most effecient way of driving GPIOs.

## Different SPI approaches

Driving SPI can be done by bitbanging, but this gives an unstable clock signal and therefor is not very reliable.
It can also be done "natively" through spidev, but setting the clock requires ioctl commands.

To use spidev, a few steps need to be followed;

1. Enable SPI on the Raspberry Pi on the console:
   ```bash
   $ sudo raspi-config
   ```
   Go to menu `5 Interfacing Options` -> `P4 SPI` and make sure it's enabled.
2. Install the PHP extension [ext-ioctl](https://github.com/CismonX/ext-ioctl). This can be done with these steps as root:
   ```bash
   # cd ~
   # mkdir ext-ioctl
   # cd ext-ioctl/
   # git clone https://github.com/CismonX/ext-ioctl .
   # apt install php7.3-dev
   # phpize
   # ./configure --enable-ioctl
   # make && make install
   # echo extension=ioctl.so > /etc/php/7.3/mods-available/ioctl.ini
   # ln -s /etc/php/7.3/mods-available/ioctl.ini /etc/php/7.3/cli/conf.d/20-ioctl.ini
   # ln -s /etc/php/7.3/mods-available/ioctl.ini /etc/php/7.3/apache2/conf.d/20-ioctl.ini
   ```

## Examples

In the examples folder, a few scripts can be found:

* **[speedtest.php](examples/speedtest.php)** - Tests between several ways of driving pins. As soon as I develop a new way, I'll add it to this test.
* **[spitest.php](examples/spitest.php)** - Simple test of using SPI
