<?php

/**
 * This file does not do anything, except tell your IDE (ex. PHPStorm) which methods the ext-ioctl extension provides.
 *
 * Installing this extension can be done with a few simple actions:
 * # cd ~
 * # mkdir ext-ioctl
 * # cd ext-ioctl/
 * # git clone https://github.com/CismonX/ext-ioctl .
 * # apt install autoconf automake libtool m4 php7.3-dev
 * # phpize
 * # ./configure --enable-ioctl
 * # make && make install
 * Now add to your php.ini file: extension=ioctl.so
 *
 * For more, see also: https://github.com/CismonX/ext-ioctl
 */

/**
 * Manipulates the underlying device parameters of special files.
 *
 * @param resource|int $fd
 * @param int $request
 * @param string $argp[optional]
 * @param int $errno[optional]
 * @return int|false
 */
function ioctl($fd, $request, $argp, &$errno)
{
}

/**
 * Allocate a string with given length without initialization.
 *
 * @param int $len
 * @return string
 */
function str_alloc($len)
{
}

/**
 * Get the address of a string.
 *
 * @param string $str
 * @return int
 */
function str2ptr($str)
{
}

/**
 * Copy data from the specified address into a string.
 *
 * @param int $ptr
 * @param int $length
 * @return string
 */
function ptr2str($ptr, $length)
{
}

/**
 * Get underlying file descriptor of a resource. Returns -1 on failure.
 *
 * @param resource $res
 * @return int
 */
function res2fd($res)
{
}
